package data;

import javafx.geometry.Point2D;
import javafx.scene.chart.XYChart;
import javafx.scene.control.Tooltip;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Stream;

/**
 * This class specifies how an algorithm will expect the dataset to be. It is
 * provided as a rudimentary structure only, and does not include many of the
 * sanity checks and other requirements of the use cases. As such, you can
 * completely write your own class to represent a set of data instances as long
 * as the algorithm can read from and write into two {@link java.util.Map}
 * objects representing the name-to-label map and the name-to-location (i.e.,
 * the x,y values) map. These two are the {@link DataSet#labels} and
 * {@link DataSet#locations} maps in this class.
 *
 * @author Ritwik Banerjee
 */
public class DataSet {

    public static class InvalidDataNameException extends Exception {

        private static final String NAME_ERROR_MSG = "All data instance names must start with the @ character.";

        public InvalidDataNameException(String name) {
            super(String.format("Invalid name '%s'." + NAME_ERROR_MSG, name));
        }
    }

    private static String nameFormatCheck(String name) throws InvalidDataNameException {
        if (!name.startsWith("@"))
            throw new InvalidDataNameException(name);
        return name;
    }

    private static Point2D locationOf(String locationString) {
        String[] coordinateStrings = locationString.trim().split(",");
        return new Point2D(Double.parseDouble(coordinateStrings[0]), Double.parseDouble(coordinateStrings[1]));
    }

    private Map<String, String>  labels;
    private Map<String, Point2D> locations;

    /** Creates an empty dataset. */
    public DataSet() {
        labels = new HashMap<>();
        locations = new HashMap<>();
    }

    public Map<String, String> getLabels()     { return labels; }

    public Map<String, Point2D> getLocations() { return locations; }

    public HashSet<String> getNonNullLabels() {
        HashSet<String> noNullsOrDupes = new HashSet<>();
        labels.forEach((n, l) -> {
            if (!(l.equals("null"))) {
               noNullsOrDupes.add(l);
            }
        });
        return noNullsOrDupes;
    }

    public double[] getMinMaxX() {
        double max = Integer.MIN_VALUE;
        double min = Integer.MAX_VALUE;

        for(Point2D point: locations.values()) {
            if(point.getX() > max) {
                max = point.getX();
            }
            if(point.getX() < min) {
                min = point.getX();
            }
        }
        return new double[]{min, max};
    }

    public void updateLabel(String instanceName, String newlabel) {
        if (labels.get(instanceName) == null)
            throw new NoSuchElementException();
        labels.put(instanceName, newlabel);
    }

    private void addInstance(String tsdLine) throws InvalidDataNameException {
        String[] arr = tsdLine.split("\t");
        labels.put(nameFormatCheck(arr[0]), arr[1]);
        locations.put(arr[0], locationOf(arr[2]));
    }

    public static DataSet fromTSDFile(Path tsdFilePath) throws IOException {
        DataSet dataset = new DataSet();
        Files.lines(tsdFilePath).forEach(line -> {
            try {
                dataset.addInstance(line);
            } catch (InvalidDataNameException e) {
                e.printStackTrace();
            }
        });
        return dataset;
    }

    public static DataSet fromTSDString(String tsdString) {
        DataSet dataset = new DataSet();
        Stream.of(tsdString.split("\n")).forEach(line -> {
            try {
                dataset.addInstance(line);
            }
            catch (InvalidDataNameException e) {
                e.printStackTrace();
            }
        });
        return dataset;
    }

    public void toChartData(XYChart<Number, Number> chart) {
        Set<String> dataLabels = new HashSet<>(labels.values());
        for (String label : dataLabels) {
            XYChart.Series<Number, Number> series = new XYChart.Series<>();
            HashMap<String, XYChart.Data> instanceMap = new HashMap<>();
            series.setName(label);
            labels.entrySet().stream().filter(entry -> entry.getValue().equals(label)).forEach(entry -> {
                Point2D point = locations.get(entry.getKey());
                XYChart.Data<Number, Number> p = new XYChart.Data<>(point.getX(), point.getY());
                instanceMap.put(entry.getKey(), p);
                series.getData().add(p);
            });
            chart.getData().add(series);
            instanceMap.keySet().forEach(k -> Tooltip.install(instanceMap.get(k).getNode(), new Tooltip(k)));
        }
    }

}