package data;

import dataprocessors.ReflectionUtilities;
import settings.AlgorithmTypes;

import java.util.Objects;

public class AlgorithmWrapper {
    private Class wrappedAlgorithm;
    private String name;
    private AlgorithmTypes type;

    public AlgorithmWrapper(Class a) {
        if(ReflectionUtilities.validateAlgorithmClass(a)) {
            this.wrappedAlgorithm = a;
        }
        else {
            throw new IllegalArgumentException();
        }
        this.name = ReflectionUtilities.trimClassName(a.getName());

        for(AlgorithmTypes t : AlgorithmTypes.values()) {
            if(t.getAlgorithmClass() == a.getSuperclass()) {
                type = t;
                break;
            }
        }
    }

    public Class getWrappedAlgorithm() {
        return wrappedAlgorithm;
    }

    public String getName() {
        return name;
    }

    public AlgorithmTypes getType() {
        return type;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        AlgorithmWrapper that = (AlgorithmWrapper) o;
        return Objects.equals(wrappedAlgorithm, that.wrappedAlgorithm) &&
                Objects.equals(name, that.name) &&
                type == that.type;
    }

    @Override
    public int hashCode() {

        return Objects.hash(wrappedAlgorithm, name, type);
    }
}
