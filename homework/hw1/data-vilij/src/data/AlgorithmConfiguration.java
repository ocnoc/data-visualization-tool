package data;

public class AlgorithmConfiguration {
    private int interval;
    private int iterations;
    private boolean continuous;

    // Clustering only below
    private int maxLabels;

    public AlgorithmConfiguration() {
        this.interval = 1;
        this.iterations = 1;
        this.continuous = true;
        this.maxLabels = 2;
    }

    public int getInterval() {
        return interval;
    }

    public void setInterval(int interval) {
        if(interval <= 0) {
            interval = 1;
        }
        this.interval = interval;
    }

    public int getIterations() {
        return iterations;
    }

    public void setIterations(int iterations) {
        if(iterations <= 0) {
            iterations = 1;
        }
        this.iterations = iterations;
    }

    public boolean isContinuous() {
        return continuous;
    }

    public void setContinuous(boolean continuous) {
        this.continuous = continuous;
    }

    public int getMaxLabels() {
        return maxLabels;
    }

    public void setMaxLabels(int maxLabels) {
        if(maxLabels < 2) {
            maxLabels = 2;
        }
        if(maxLabels > 4) {
            maxLabels = 4;
        }
        this.maxLabels = maxLabels;
    }

}
