package dataprocessors;

import algorithms.Algorithm;
import data.AlgorithmConfiguration;
import data.AlgorithmWrapper;
import data.DataSet;
import javafx.scene.Cursor;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart;
import javafx.scene.control.TextArea;
import settings.AlgorithmTypes;
import settings.AppPropertyTypes;
import settings.ApplicationState;
import ui.AppUI;
import vilij.components.DataComponent;
import vilij.components.Dialog;
import vilij.components.ErrorDialog;
import vilij.propertymanager.PropertyManager;
import vilij.templates.ApplicationTemplate;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.FileWriter;
import java.nio.file.Path;
import java.util.*;

import static settings.AppPropertyTypes.*;
import static vilij.settings.PropertyTypes.*;

/**
 * This is the concrete application-specific implementation of the data component defined by the Vilij framework.
 *
 * @author Ritwik Banerjee
 * @see DataComponent
 */
public class AppData implements DataComponent {

    private TSDProcessor        processor;
    private ApplicationTemplate applicationTemplate;
    private ArrayList<String>   loadedData = new ArrayList<>();
    private String              loadedTsd;
    private HashMap<AlgorithmWrapper, AlgorithmConfiguration> configurations = new HashMap<>();
    private Algorithm           loadedAlgorithm;
    private XYChart.Series<Number, Number> classifierSeries;
    private DataSet             originalData;

    public AppData(ApplicationTemplate applicationTemplate) {
        this.processor = new TSDProcessor();
        this.applicationTemplate = applicationTemplate;
    }

    @Override
    public void loadData(Path dataFilePath) {
        clear();
        ErrorDialog error = ((ErrorDialog)applicationTemplate.getDialog(Dialog.DialogType.ERROR));
        PropertyManager manager = applicationTemplate.manager;
        StringBuilder dataInfo = new StringBuilder();
        int instanceCount, labelCount;
        Set<String> labels = new HashSet<>();
        StringBuilder dataCheck = new StringBuilder();
        loadedData.clear();
        StringBuilder manualEdit = new StringBuilder();
        try {
            BufferedReader reader = new BufferedReader(new FileReader(dataFilePath.toString()));
            reader.lines().forEach(line -> {
                loadedData.add(line + System.lineSeparator());
                dataCheck.append(line).append(System.lineSeparator());
                    });
            reader.close();
        }
        catch(IOException e) {
            error.show(manager.getPropertyValue(LOAD_ERROR_TITLE.name()), manager.getPropertyValue(LOAD_ERROR_MSG.name())
                    + manager.getPropertyValue(SPECIFIED_FILE.name()));
            loadedData.clear();
            loadedTsd = null;
            clear();
            return;
        }
        try {
            processor.processString(dataCheck.toString());
            loadedTsd = dataCheck.toString();
            instanceCount = loadedData.size();
        }
        catch (Exception e) {
            error.show(manager.getPropertyValue(LOAD_ERROR_TITLE.name()), e.getMessage());
            loadedData.clear();
            loadedTsd = null;
            clear();
            return;
        }
        if(loadedData.size() > 10) {
            while (manualEdit.toString().split(System.lineSeparator()).length < 10) {
                manualEdit.append(loadedData.get(0));
                loadedData.remove(0);
            }
            error.show(manager.getPropertyValue(MANY_LINES_TITLE.name()), String.format(manager.getPropertyValue(MANY_LINES.name()),
                    loadedData.size() + 10));
        }
        else {
            while(loadedData.size() > 0) {
                manualEdit.append(loadedData.get(0));
                loadedData.remove(0);
            }
        }
        try {
            originalData = DataSet.fromTSDFile(dataFilePath);
        } catch (IOException e) {
            e.printStackTrace();
        }
        processor.getDataLabels().forEach((name, label) -> labels.add(label));
        labelCount = labels.size();
        ((AppUI)applicationTemplate.getUIComponent()).disableSave();
        dataInfo.append(String.format(manager.getPropertyValue(AppPropertyTypes.METADATA_MAIN.name()), instanceCount, labelCount));
        dataInfo.append("\n");
        labels.forEach(label -> {
            dataInfo.append(String.format(manager.getPropertyValue(AppPropertyTypes.METADATA_LABELS.name()), label));
            dataInfo.append("\n");
        });
        dataInfo.append(String.format(manager.getPropertyValue(AppPropertyTypes.METADATA_SOURCE.name()), dataFilePath.toString()));
        ((AppUI)applicationTemplate.getUIComponent()).loadedData(dataInfo.toString(), manualEdit.toString());
    }

    public void loadData(String dataString) {
        ErrorDialog error = ((ErrorDialog)applicationTemplate.getDialog(Dialog.DialogType.ERROR));
        PropertyManager manager = applicationTemplate.manager;
        clear();
        StringBuilder data = new StringBuilder();
        try {
            data.append(dataString);
            if(loadedData.size() != 0) {
                if(!(data.toString().substring(data.toString().length() - 1).equals(System.lineSeparator()))) {
                    data.append(System.lineSeparator());
                }
                loadedData.forEach(data::append);
            }
            processor.processString(data.toString());
            originalData = DataSet.fromTSDString(data.toString());
            displayData();
        }
        catch(Exception e) {
            error.show(manager.getPropertyValue(DISPLAY_ERROR_TITLE.name()), e.getMessage());
            ((AppUI)applicationTemplate.getUIComponent()).getChart().getData().clear();
        }
    }

    @Override
    public void saveData(Path dataFilePath) {
        File file = dataFilePath.toFile();
        PropertyManager manager = applicationTemplate.manager;
        ErrorDialog error = ((ErrorDialog)applicationTemplate.getDialog(Dialog.DialogType.ERROR));
        StringBuilder data = new StringBuilder(((AppUI)applicationTemplate.getUIComponent()).getTextArea().getText());
        if(!(data.toString().substring(data.toString().length() - 1).equals(System.lineSeparator()))) {
            data.append(System.lineSeparator());
        }
        if(loadedData.size() != 0) {
            loadedData.forEach(data::append);
        }

        try {
            FileWriter writer = new FileWriter(file);
            writer.write(data.toString());
            writer.close();
            ((AppUI)applicationTemplate.getUIComponent()).disableSave();
        }
        catch (IOException e) {
            error.show(manager.getPropertyValue(SAVE_ERROR_TITLE.name()), manager.getPropertyValue(SAVE_ERROR_MSG.name()));
        }
    }

    public void saveData(Path dataFilePath, StringBuilder data) {
        File file = dataFilePath.toFile();
        if(!(data.toString().substring(data.toString().length() - 1).equals(System.lineSeparator()))) {
            data.append(System.lineSeparator());
        }
        if(loadedData.size() != 0) {
            loadedData.forEach(data::append);
        }

        try {
            FileWriter writer = new FileWriter(file);
            writer.write(data.toString());
            writer.close();
        }
        catch (IOException e) {
            // do nothing cuz im dumb
        }
    }

    @Override
    public void clear() {
        processor.clear();
    }

    public void displayData() {
        double average = 0;
        XYChart.Series<Number, Number>      averageSeries = new XYChart.Series<>();

        ((AppUI)applicationTemplate.getUIComponent()).getChart().getData().clear();
        processor.toChartData(((AppUI) applicationTemplate.getUIComponent()).getChart());

        XYChart<Number, Number> chart = ((AppUI)applicationTemplate.getUIComponent()).getChart();

        ArrayList<XYChart.Data> points = new ArrayList<>();
        ((AppUI)applicationTemplate.getUIComponent()).getChart().getData().forEach(entry -> points.addAll(entry.getData()));
        double xmin = Double.MAX_VALUE;
        double xmax = 0;
        for (XYChart.Data point : points) {
            point.getNode().setOnMouseEntered(e -> applicationTemplate.getUIComponent().getPrimaryScene().setCursor(Cursor.CROSSHAIR));
            point.getNode().setOnMouseExited(e -> applicationTemplate.getUIComponent().getPrimaryScene().setCursor(Cursor.DEFAULT));
            average += (double)point.getYValue();
            if((double)point.getXValue() < xmin) {
                xmin = (double)point.getXValue();
            }
            if((double)point.getXValue() > xmax) {
                xmax = (double)point.getXValue();
            }
        }
        NumberAxis xAxis = ((NumberAxis)chart.getXAxis());
        NumberAxis yAxis = ((NumberAxis)chart.getYAxis());
        xAxis.setAutoRanging(true);
        yAxis.setAutoRanging(true);
        xAxis.setForceZeroInRange(false);
        yAxis.setForceZeroInRange(false);
        //xAxis.setLowerBound(xmin - 5);
        //xAxis.setUpperBound(xmax + 5);

        (averageSeries.getData()).add(new XYChart.Data<>(xmin, average));
        (averageSeries.getData()).add(new XYChart.Data<>(xmax, average));
    }

    public ArrayList<String> getLoadedData() {
        return loadedData;
    }

    public void refillText() {
        TextArea text = ((AppUI)applicationTemplate.getUIComponent()).getTextArea();
        StringBuilder newText = new StringBuilder(text.getText());

        while(text.getText().split(System.lineSeparator()).length < 10 && loadedData.size() != 0) {
            newText.append(loadedData.get(0));
            loadedData.remove(0);
            text.textProperty().setValue(newText.toString());
        }
    }

    public AlgorithmConfiguration getAlgorithmConfig (AlgorithmWrapper a) {
        return configurations.getOrDefault(a, null);
    }

    public void addAlgorithmConfiguration(AlgorithmWrapper a, AlgorithmConfiguration c) {
        configurations.put(a, c);
    }

    public boolean testData(String dataString) {
        ErrorDialog error = ((ErrorDialog)applicationTemplate.getDialog(Dialog.DialogType.ERROR));
        PropertyManager manager = applicationTemplate.manager;
        loadedTsd = dataString;
        clear();
        StringBuilder data = new StringBuilder();
        try {
            data.append(dataString);
            if(loadedData.size() != 0) {
                if(!(data.toString().substring(data.toString().length() - 1).equals(System.lineSeparator()))) {
                    data.append(System.lineSeparator());
                }
                loadedData.forEach(data::append);
            }
            processor.processString(data.toString());
            return true;
        }
        catch(Exception e) {
            error.show(manager.getPropertyValue(DISPLAY_ERROR_TITLE.name()), e.getMessage());
            ((AppUI)applicationTemplate.getUIComponent()).getChart().getData().clear();
        }
        return false;
    }

    public String calculateMetaData(String tsdString) {
        PropertyManager manager = PropertyManager.getManager();
        DataSet data = DataSet.fromTSDString(tsdString);
        StringBuilder dataInfo = new StringBuilder();
        int labelCount = data.getNonNullLabels().size();
        HashSet<String> labels = data.getNonNullLabels();
        int instances = data.getLabels().size();
        dataInfo.append(String.format(manager.getPropertyValue(AppPropertyTypes.METADATA_MAIN.name()), instances, labelCount));
        dataInfo.append("\n");
        labels.forEach(label -> {
            dataInfo.append(String.format(manager.getPropertyValue(AppPropertyTypes.METADATA_LABELS.name()), label));
            dataInfo.append("\n");
        });
        return dataInfo.toString();
    }

    public int countLabels(String tsdString) {
        DataSet d = DataSet.fromTSDString(tsdString);
        return d.getNonNullLabels().size();
    }

    public String getLoadedTsd() {
        return loadedTsd;
    }

    public void prepareAlgorithm(AlgorithmWrapper a) throws IllegalArgumentException {
        if(getAlgorithmConfig(a) == null ) {
            throw new IllegalArgumentException();
        }
        loadedAlgorithm = AlgorithmFactory.getAlgorithm(a, getAlgorithmConfig(a), originalData);
        if(loadedAlgorithm == null) {
            System.out.println("UH OH");
            throw new IllegalArgumentException();
        }
        loadedAlgorithm.setConnectedUI(applicationTemplate);
    }

    public void runAlgorithm() {
        Thread th = new Thread(loadedAlgorithm);
        th.setDaemon(true);
        th.start();
        ((AppUI)applicationTemplate.getUIComponent()).setApplicationState(ApplicationState.ALGORITHM_RUNNING);
    }

    public void processClassifierOutput(List<Integer> output, DataSet data) {
        ((AppUI)applicationTemplate.getUIComponent()).getChart().getData().remove(classifierSeries);
        double[] minmax = data.getMinMaxX();

        double a = output.get(0);
        double b = output.get(1);
        double c = output.get(2);

        double minx = minmax[0];
        double maxx = minmax[1];

        if(minx == maxx) {
            minx-= 10;
            maxx+= 10;
        }

        if(b == 0) {
            b = 1;
        }

        double miny = (((-1) * a * minx) - c) / b;
        double maxy = (((-1) * a * maxx) - c) / b;

        classifierSeries = new XYChart.Series<>();
        classifierSeries.getData().add(new XYChart.Data<>(minx, miny));
        classifierSeries.getData().add(new XYChart.Data<>(maxx, maxy));
        ((AppUI)applicationTemplate.getUIComponent()).getChart().getData().add(classifierSeries);
        classifierSeries.getNode().getStyleClass().add("classifier-series");
        classifierSeries.getData().forEach(n -> n.getNode().getStyleClass().add("classifier-node"));

    }

    public void processClusteringOutput(DataSet output) {
        XYChart chart = ((AppUI)applicationTemplate.getUIComponent()).getChart();
        chart.getData().clear();
        output.toChartData(chart);
    }

  /*  public HashSet<Algorithm> generateAlgorithmSet(AlgorithmTypes type) {
        ClassLoader.getRe
    }*/

    public void algorithmFinished() {
        ((AppUI)applicationTemplate.getUIComponent()).setApplicationState(ApplicationState.ALGORITHM_FINISHED);
    }

    public void algorithmPause() {
        ((AppUI)applicationTemplate.getUIComponent()).setApplicationState(ApplicationState.ALGORITHM_PAUSED);
    }

    public void algorithmResume() {
        ((AppUI)applicationTemplate.getUIComponent()).setApplicationState(ApplicationState.ALGORITHM_RUNNING);
        loadedAlgorithm.resume();
    }

    public void algorithmStop() {
        loadedAlgorithm.stop();
    }

    public void loadOriginalData(String tsdString) {
        originalData = DataSet.fromTSDString(tsdString);
    }

    public boolean algorithmTypeValidation(AlgorithmTypes type) {
        int labels = originalData.getNonNullLabels().size();
        int instances = originalData.getLocations().size();

        return instances >= type.getMinInstances() && labels >= type.getMinLabels();
    }

}
