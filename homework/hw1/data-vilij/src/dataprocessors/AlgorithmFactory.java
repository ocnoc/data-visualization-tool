package dataprocessors;

import algorithms.Algorithm;
import data.AlgorithmConfiguration;
import data.AlgorithmWrapper;
import data.DataSet;
import settings.AlgorithmTypes;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;

public class AlgorithmFactory {
    public static Algorithm getAlgorithm(AlgorithmWrapper a, AlgorithmConfiguration config, DataSet data) {
        AlgorithmTypes algoType = a.getType();
        Constructor konstructor;
        Class<?> algorithmClass = a.getWrappedAlgorithm();
        konstructor = algorithmClass.getConstructors()[0];
        if(algoType == AlgorithmTypes.CLASSIFICATION) {
            try {
                return (Algorithm)konstructor.newInstance(data, config.getIterations(), config.getInterval(), config.isContinuous());
            } catch (InstantiationException | InvocationTargetException | IllegalAccessException e) {
                e.printStackTrace();
            }
        }
        else if(algoType == AlgorithmTypes.CLUSTERING) {
            try {
                return (Algorithm)konstructor.newInstance(data, config.getIterations(), config.getInterval(), config.isContinuous(), config.getMaxLabels());
            } catch (InstantiationException | InvocationTargetException | IllegalAccessException e) {
                e.printStackTrace();
            }
        }
        return null;
    }
}
