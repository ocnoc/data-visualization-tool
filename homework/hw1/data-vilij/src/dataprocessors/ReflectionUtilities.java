package dataprocessors;

import algorithms.Algorithm;
import data.AlgorithmWrapper;
import settings.AlgorithmTypes;
import settings.AppPropertyTypes;
import vilij.propertymanager.PropertyManager;

import java.io.File;
import java.net.URL;
import java.util.HashSet;

public class ReflectionUtilities {

    // Based on code by Jaikiran Pai found online for finding all classes in a package
    static private HashSet<Class> getAllClasses(String packageName) {
        try{

            HashSet<Class> classSet = new HashSet<>();
            File directory;

            try {
                // Get our current thread
                Thread currThread = Thread.currentThread();

                // Get the class loader for it
                ClassLoader loader = currThread.getContextClassLoader();

                // Grab a URL pointing to the resources folder for this classLoader, and navigate to a specific package folder (replacing '.' with '/' for file navigation
                URL rsc = loader.getResource(packageName.replace('.', '/'));

                // Get the file of the URL as a directory
                if (rsc != null) {
                    directory = new File(rsc.getFile());
                }
                else {
                    return null;
                }
            } catch(NullPointerException e) {
                e.printStackTrace();
                return null;
            }
            if(directory.exists()) {
                // Get all files in the package folder
                String[] files = directory.list();
                if (files != null) {
                    for (String file : files) {
                        // Grab only files ending in the .class extension
                        if (file.endsWith(".class")) {
                            // Add the class. Removes the .class extension so Java doesnt throw a fit
                            classSet.add(Class.forName(packageName + '.' + file.substring(0, file.length() - 6)));
                        }
                    }
                }
            }
            else {
                System.out.println(PropertyManager.getManager().getPropertyValue(AppPropertyTypes.DIRECTORY_ERROR_MSG.name()));
            }

            return classSet;

        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static HashSet<AlgorithmWrapper> getAlgorithms(AlgorithmTypes type) {

        String packagePath = "algorithms." +
                type.getAlgorithmPackage();
        HashSet<Class> foundClasses = getAllClasses(packagePath);
        HashSet<AlgorithmWrapper> algorithmSet = new HashSet<>();

        if (foundClasses != null) {
            for(Class c : foundClasses) {
                if(c.getSuperclass() == type.getAlgorithmClass()) {
                    algorithmSet.add(new AlgorithmWrapper(c));
                    //System.out.println(c.getName());
                }
                else {
                    System.out.println(c.getName() + " is not valid!");
                }
            }
        }
        else {
            return null;
        }
        return algorithmSet;

    }



    public static String trimClassName(String name) {
        String[] trimmer = name.split("\\.", 0);
        return trimmer[(trimmer.length)-1];
    }

    public static boolean validateAlgorithmClass(Class<?> a) {
        return (Algorithm.class).isAssignableFrom(a);
    }

}
