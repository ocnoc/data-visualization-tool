package dataprocessors;

import javafx.geometry.Point2D;
import javafx.scene.chart.XYChart;
import javafx.scene.control.Tooltip;
import vilij.propertymanager.PropertyManager;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Stream;

import static settings.AppPropertyTypes.FORMAT_ERROR_MESSAGE;
import static settings.AppPropertyTypes.NAME_DUPE_MESSAGE;
import static settings.AppPropertyTypes.NAME_ERROR_MESSAGE;

/**
 * The data files used by this data visualization applications follow a tab-separated format, where each data point is
 * named, labeled, and has a specific location in the 2-dimensional X-Y plane. This class handles the parsing and
 * processing of such data. It also handles exporting the data to a 2-D plot.
 * <p>
 * A sample file in this format has been provided in the application's <code>resources/data</code> folder.
 *
 * @author Ritwik Banerjee
 * @see XYChart
 */
public final class TSDProcessor {
    private static final PropertyManager manager = PropertyManager.getManager();
    private static final String FORMAT_ERROR_MSG = manager.getPropertyValue(FORMAT_ERROR_MESSAGE.name());

    public static class InvalidDataNameException extends Exception {

        private static final String NAME_DUPE = manager.getPropertyValue(NAME_DUPE_MESSAGE.name());
        private static final String NAME_ERROR = manager.getPropertyValue(NAME_ERROR_MESSAGE.name());

        public InvalidDataNameException(String name, int lineNum, boolean duplicate) {
            super(String.format(duplicate ? NAME_ERROR : NAME_DUPE, name, lineNum));
        }

    }

    private Map<String, String> dataLabels;
    private Map<String, Point2D> dataPoints;

    public TSDProcessor() {
        dataLabels = new HashMap<>();
        dataPoints = new HashMap<>();
    }

    /**
     * Processes the data and populated two {@link Map} objects with the data.
     *
     * @param tsdString the input data provided as a single {@link String}
     * @throws Exception if the input string does not follow the <code>.tsd</code> data format
     */
    public void processString(String tsdString) throws Exception {
        AtomicInteger lineNum = new AtomicInteger(0);
        ArrayList<String> usedName = new ArrayList<>();
        AtomicBoolean hadAnError   = new AtomicBoolean(false);
        StringBuilder errorMessage = new StringBuilder();
        Stream.of(tsdString.split("\n"))
              .map(line -> Arrays.asList(line.split("\t")))
              .forEach(list -> {
                  try {
                      lineNum.incrementAndGet();
                      String   name  = checkedname(list.get(0), lineNum.get(), usedName);
                      usedName.add(list.get(0));
                      String   label = list.get(1);
                      if(label.equals("")) {
                          throw new Exception();
                      }
                      String[] pair  = list.get(2).split(",");
                      Point2D  point = new Point2D(Double.parseDouble(pair[0]), Double.parseDouble(pair[1]));
                      dataLabels.put(name, label);
                      dataPoints.put(name, point);
                  }
                  catch (Exception e) {
                      if(e instanceof InvalidDataNameException) {
                          errorMessage.setLength(0);
                          errorMessage.append(e.getMessage());
                          hadAnError.set(true);
                      }
                      else {
                          errorMessage.setLength(0);
                          errorMessage.append(FORMAT_ERROR_MSG).append(lineNum);
                          hadAnError.set(true);
                      }
                  }
              });
        if (errorMessage.length() > 0)
            throw new Exception(errorMessage.toString());
    }

    /**
     * Exports the data to the specified 2-D chart.
     *
     * @param chart the specified chart
     */
    void toChartData(XYChart<Number, Number> chart) {
        Set<String> labels = new HashSet<>(dataLabels.values());
        for (String label : labels) {
            XYChart.Series<Number, Number> series = new XYChart.Series<>();
            HashMap<String, XYChart.Data> instanceMap = new HashMap<>();
            series.setName(label);
            dataLabels.entrySet().stream().filter(entry -> entry.getValue().equals(label)).forEach(entry -> {
                Point2D point = dataPoints.get(entry.getKey());
                XYChart.Data<Number, Number> p = new XYChart.Data<>(point.getX(), point.getY());
                instanceMap.put(entry.getKey(), p);
                series.getData().add(p);
            });
            chart.getData().add(series);
            instanceMap.keySet().forEach(k -> Tooltip.install(instanceMap.get(k).getNode(), new Tooltip(k)));
        }
    }

    void clear() {
        dataPoints.clear();
        dataLabels.clear();
    }

    private String checkedname(String name, int lineNum, ArrayList<String> usedNames) throws InvalidDataNameException {
        if (!name.startsWith("@"))
            throw new InvalidDataNameException(name, lineNum, true);
        else if(usedNames.contains(name)){
            throw new InvalidDataNameException(name, lineNum, false);
        }
        return name;
    }

    public Map<String, Point2D> getDataPoints() { return dataPoints; }
    public Map<String, String> getDataLabels() {
        return dataLabels;
    }
}
