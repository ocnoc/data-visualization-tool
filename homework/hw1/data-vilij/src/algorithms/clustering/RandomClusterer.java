package algorithms.clustering;

import algorithms.Clusterer;
import data.DataSet;
import dataprocessors.AppData;
import javafx.application.Platform;
import javafx.geometry.Point2D;
import vilij.templates.ApplicationTemplate;

import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicBoolean;

public class RandomClusterer extends Clusterer {

    private DataSet dataset;

    private final int maxIterations;
    private final int updateInterval;
    private final int maxLabels;
    private final AtomicBoolean continuous;
    private final AtomicBoolean stopped = new AtomicBoolean(false);

    private ApplicationTemplate applicationTemplate;

    public RandomClusterer(DataSet dataset, int maxIterations, int updateInterval, boolean continuous, int numberOfClusters) {
        super(numberOfClusters);
        this.dataset = dataset;
        this.maxIterations = maxIterations;
        this.updateInterval = updateInterval;
        this.maxLabels = numberOfClusters;
        this.continuous = new AtomicBoolean(continuous);

    }

    @Override
    public int getMaxIterations() {
        return maxIterations;
    }

    @Override
    public int getUpdateInterval() {
        return updateInterval;
    }

    @Override
    public boolean tocontinue() {
        return continuous.get();
    }

    @Override
    public void resume() {
        synchronized (this) {
            continuous.set(true);
            this.notifyAll();
        }
    }

    @Override
    public void pause() {
        try {
            ((AppData) applicationTemplate.getDataComponent()).algorithmPause();
            synchronized (this) {
                wait();
            }
        }
        catch (InterruptedException e) {
            // nothing
        }
    }

    @Override
    public void stop() {
        this.stopped.set(true);
    }

    @Override
    public void setConnectedUI(ApplicationTemplate applicationTemplate) {
        this.applicationTemplate = applicationTemplate;
    }

    @Override
    public void run() {
        for(int i=0;i<=maxIterations;i++) {
            if(stopped.get()) {
                break;
            }
            randomizeLabels();

            if(i % updateInterval == 0 || i == maxIterations) {
                Platform.runLater(() -> ((AppData)applicationTemplate.getDataComponent()).processClusteringOutput(dataset));

                try {
                    Thread.sleep(1000);
                }
                catch(Exception e) {
                    // do nothing
                }
                if(!(tocontinue()) && !(i == maxIterations)) {
                    while (!(tocontinue())) {
                        pause();
                    }
                    continuous.set(false);
                }
            }
        }
        ((AppData)applicationTemplate.getDataComponent()).algorithmFinished();
    }

    private void randomizeLabels() {
        Random r = new Random();
        dataset.getLabels().forEach((k, v) -> {
            dataset.updateLabel(k, String.valueOf(r.nextInt(maxLabels)));
        });
    }
}
