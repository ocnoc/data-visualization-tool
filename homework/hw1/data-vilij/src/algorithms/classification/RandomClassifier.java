package algorithms.classification;

import algorithms.Classifier;
import data.DataSet;
import dataprocessors.AppData;
import javafx.application.Platform;
import vilij.templates.ApplicationTemplate;

import java.io.IOException;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.Random;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 * @author Ritwik Banerjee
 */
public class RandomClassifier extends Classifier {

    private static final Random RAND = new Random();

    @SuppressWarnings("FieldCanBeLocal")
    // this mock classifier doesn't actually use the data, but a real classifier will
    private DataSet dataset;

    private final int maxIterations;
    private final int updateInterval;

    private ApplicationTemplate applicationTemplate;

    // currently, this value does not change after instantiation
    private final AtomicBoolean tocontinue;
    private final AtomicBoolean stop = new AtomicBoolean(false);

    @Override
    public int getMaxIterations() {
        return maxIterations;
    }

    @Override
    public int getUpdateInterval() {
        return updateInterval;
    }

    @Override
    public boolean tocontinue() {
        return tocontinue.get();
    }

    public RandomClassifier(DataSet dataset,
                            int maxIterations,
                            int updateInterval,
                            boolean tocontinue) {
        this.dataset = dataset;
        this.maxIterations = maxIterations;
        this.updateInterval = updateInterval;
        this.tocontinue = new AtomicBoolean(tocontinue);
    }

    @Override
    public void run() {
        for (int i = 1; i <= maxIterations; i++) {
            if(stop.get()) {
                break;
            }
            int xCoefficient =  new Long(-1 * Math.round((2 * RAND.nextDouble() - 1) * 10)).intValue();
            int yCoefficient = 10;
            int constant     = RAND.nextInt(11);

            // this is the real output of the classifier
            output = Arrays.asList(xCoefficient, yCoefficient, constant);

            // everything below is just for internal viewing of how the output is changing
            // in the final project, such changes will be dynamically visible in the UI
            if (i % updateInterval == 0 || i == maxIterations) {
                System.out.printf("Iteration number %d: ", i); //
                flush();
                Platform.runLater(() -> {
                    AppData data = ((AppData)applicationTemplate.getDataComponent());
                    data.processClassifierOutput(output, dataset);
                });
                try {
                    Thread.sleep(1000);
                }
                catch(Exception e) {
                    // do nothing
                }
                if(!(tocontinue()) && !(i == maxIterations)) {
                    while (!(tocontinue())) {
                        pause();
                    }
                    tocontinue.set(false);
                }
            }
            if (i > maxIterations * .6 && RAND.nextDouble() < 0.05) {
                System.out.printf("Iteration number %d: ", i);
                flush();
                Platform.runLater(() -> {
                    AppData data = ((AppData)applicationTemplate.getDataComponent());
                    data.processClassifierOutput(output, dataset);
                });
                try {
                    Thread.sleep(1000);
                }
                catch(Exception e) {
                    // do nothing
                }
                break;
            }
        }
        ((AppData)applicationTemplate.getDataComponent()).algorithmFinished();
    }

    // for internal viewing only
    protected void flush() {
        System.out.printf("%d\t%d\t%d%n", output.get(0), output.get(1), output.get(2));
    }

    public void resume() {
        synchronized (this) {
            tocontinue.set(true);
            this.notifyAll();
        }
    }

    public void pause() {
        try {
            ((AppData) applicationTemplate.getDataComponent()).algorithmPause();
            synchronized (this) {
                wait();
            }
        }
        catch (InterruptedException e) {
            // nothing
        }
    }

    public void stop() {
        stop.set(true);
    }

    public void setConnectedUI(ApplicationTemplate applicationTemplate) {
        this.applicationTemplate = applicationTemplate;
    }

    /** A placeholder main method to just make sure this code runs smoothly */
    public static void main(String... args) throws IOException {
        DataSet          dataset    = DataSet.fromTSDFile(Paths.get("/home/m/example.tsd"));
        RandomClassifier classifier = new RandomClassifier(dataset, 1000, 5, true);
        new Thread(classifier).run();
    }
}
