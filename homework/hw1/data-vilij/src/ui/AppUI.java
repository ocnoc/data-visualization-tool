package ui;

import actions.AppActions;
import algorithms.Algorithm;
import data.AlgorithmConfiguration;
import data.AlgorithmWrapper;
import data.DataSet;
import dataprocessors.AppData;
import dataprocessors.ReflectionUtilities;
import javafx.application.Platform;
import javafx.beans.binding.BooleanBinding;
import javafx.geometry.Insets;
import javafx.scene.chart.LineChart;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart;
import javafx.scene.control.*;
import javafx.scene.layout.*;
import javafx.scene.shape.Line;
import javafx.stage.Stage;
import settings.AlgorithmTypes;
import settings.AppPropertyTypes;
import settings.ApplicationState;
import vilij.components.ConfirmationDialog;
import vilij.components.Dialog;
import vilij.components.ErrorDialog;
import vilij.propertymanager.PropertyManager;
import vilij.templates.ApplicationTemplate;
import vilij.templates.UITemplate;

import java.io.IOException;
import java.util.HashMap;
import java.util.HashSet;

import static settings.AppPropertyTypes.*;
import static vilij.settings.PropertyTypes.CSS_RESOURCE_PATH;
import static vilij.settings.PropertyTypes.GUI_RESOURCE_PATH;

/**
 * This is the application's user interface implementation.
 *
 * @author Ritwik Banerjee
 */
public final class AppUI extends UITemplate {

    /** The application to which this class of actions belongs. */
    ApplicationTemplate applicationTemplate;
    private static final String          SEPARATOR = "/";

    @SuppressWarnings("FieldCanBeLocal")
    private boolean                      firstData;      // Have we started data yet?
    private NumberAxis                   xAxis;          // Auto scaling axis for inputted data
    private NumberAxis                   yAxis;          // Auto-scaling axis for inputted data
    private Pane left;                                   // Vertical box to hold the left area, title, and display button
    private Button                       scrnshotButton; // toolbar button to take a screenshot of the data
    private XYChart<Number, Number>      chart;          // the chart where data will be displayed
    private Button                       displayButton;  // workspace button to display data on the chart
    private TextArea                     textArea;       // left area for new data input
    private boolean                      hasNewText;     // whether or not the left area has any new data since last display
    private String                       scrnIconPath;   // Icon path to screenshot button
    private String                       configIconPath; // Icon path to Configuration button
    private BooleanBinding               textAreaEmpty;  // Boolean to auto-check if the left area is empty
    private boolean                      hasSaved;       // Boolean to check if we have new data since last save
    private ConfirmationDialog           alert;          // Confirmation to be used by buttons
    private ErrorDialog                  errorDialog;    // A dialog to be used when error thrown
    private CheckBox                     readOnlyButton; // a button controlling whether the left arrea is read only
    private Pane                         belowText;      // Hbox lining up read only and display button
    private String                       chartCSS;       // String leading to the chart's css file
    private Region                       spacer1,spacer2;// Spacers to keep everything evenly spaced :)
    private ChoiceBox<AlgorithmTypes>    algoTypeSelect; // Select Algorithm Type
    private Label                        dataInfo;       // Stores info on the currently loaded data
    private ToggleGroup                  algoToggleGroup;// Group holding radio buttons for selecting algorithms
    private Pane                         algoSelect;     // Box for holding the visual radio buttons
    private AlgorithmConfigurationWindow configurationWindow; // Algorithm configuration window
    private Button                       runButton;      // Button for running algorithm
    private boolean                      loadedData;     // Is the data from a file?
    private AlgorithmWrapper             selectedAlgorithm;// The lagorithm selected
    private ApplicationState             applicationState;// Current state of the application
    private Pane                         algorithmControls;// Box holding algroithm controls (run, resume, stop)
    private Button                       resumeButton;   // Button to resume a paused non-contiuous algorithm
    private Button                       stopButton;     // Button to prematurely stop the algorithm
    private Label                        stopping;
    private HashSet<AlgorithmWrapper>    algorithmSet;   // Set of algorithms

    public XYChart<Number, Number> getChart() { return chart; }

    public AppUI(Stage primaryStage, ApplicationTemplate applicationTemplate) {
        super(primaryStage, applicationTemplate);
        this.applicationTemplate = applicationTemplate;
    }

    @Override
    protected void setResourcePaths(ApplicationTemplate applicationTemplate) {
        PropertyManager manager = applicationTemplate.manager;
        super.setResourcePaths(applicationTemplate);
        String iconsPath = SEPARATOR + String.join(SEPARATOR,
                manager.getPropertyValue(ICON_RESOURCE_PATH.name()));
        scrnIconPath = String.join(SEPARATOR, iconsPath, manager.getPropertyValue(SCREENSHOT_ICON.name()));
        configIconPath = String.join(SEPARATOR, iconsPath, manager.getPropertyValue(CONFIGURATION_ICON.name()));
        chartCSS = String.join(SEPARATOR, manager.getPropertyValue(GUI_RESOURCE_PATH.name()),
                manager.getPropertyValue(CSS_RESOURCE_PATH.name()),
                manager.getPropertyValue(CHART_CSS.name()));
    }

    @Override
    protected void setToolBar(ApplicationTemplate applicationTemplate) {
        PropertyManager manager = applicationTemplate.manager;
        super.setToolBar(applicationTemplate);
        scrnshotButton = setToolbarButton(scrnIconPath, manager.getPropertyValue(SCREENSHOT_TOOLTIP.name()), true);
        toolBar.getItems().add(scrnshotButton);
    }

    @Override
    protected void setToolbarHandlers(ApplicationTemplate applicationTemplate) {
        PropertyManager manager = applicationTemplate.manager;
        applicationTemplate.setActionComponent(new AppActions(applicationTemplate));
        newButton.setOnAction(e -> applicationTemplate.getActionComponent().handleNewRequest());
        saveButton.setOnAction(e -> applicationTemplate.getActionComponent().handleSaveRequest());
        loadButton.setOnAction(e -> applicationTemplate.getActionComponent().handleLoadRequest());
        exitButton.setOnAction(e -> applicationTemplate.getActionComponent().handleExitRequest());
        printButton.setOnAction(e -> applicationTemplate.getActionComponent().handlePrintRequest());
        scrnshotButton.setOnAction(e -> {
            try {
                ((AppActions)applicationTemplate.getActionComponent()).handleScreenshotRequest();
            }
            catch(IOException i) {
                ErrorDialog error = ((ErrorDialog)applicationTemplate.getDialog(Dialog.DialogType.ERROR));
                error.show(manager.getPropertyValue(SCREENSHOT_ERROR_TITLE.name()), manager.getPropertyValue(SCREENSHOT_ERROR.name()));
            }
        });
    }

    @Override
    public void initialize() {
        layout();
        setWorkspaceActions();

    }

    @Override
    public void clear() {
        chart.getData().clear();
        textArea.clear();
        hasNewText = true;
    }

    private void layout() {
        firstData = false;
        PropertyManager manager = applicationTemplate.manager;
        workspace = new HBox(10);
        belowText = new HBox(3);
        left = new VBox(10);
        textArea = new TextArea();
        displayButton = new Button(manager.getPropertyValue(DISPLAY_BUTTON.name()));
        xAxis = new NumberAxis();
        yAxis = new NumberAxis();
        Line verticalDivider = new Line();
        spacer1 = new Region();
        spacer2 = new Region();
        algoTypeSelect = new ChoiceBox<>();
        configurationWindow = AlgorithmConfigurationWindow.getConfigWindow();
        loadedData = false;
        applicationState = ApplicationState.SETUP;
        algoSelect = new Pane();
        stopButton = new Button();

        configurationWindow.init(applicationTemplate.getUIComponent().getPrimaryWindow());

        algoTypeSelect.getItems().setAll(AlgorithmTypes.values());
        algoTypeSelect.setTooltip(new Tooltip(manager.getPropertyValue(ALGORITHM_TYPE_SELECTION_TOOLTIP.name())));

        verticalDivider.setEndY(applicationTemplate.getUIComponent().getPrimaryScene().getHeight());
        verticalDivider.setStartY(0);

        HBox.setHgrow(spacer1, Priority.ALWAYS);
        HBox.setHgrow(spacer2, Priority.ALWAYS);

        xAxis.setAutoRanging(true);
        yAxis.setAutoRanging(true);

        chart = new LineChart<>(xAxis, yAxis);
        chart.setTitle(manager.getPropertyValue(CHART_TITLE.name()));
        chart.setHorizontalGridLinesVisible(false);
        chart.setVerticalGridLinesVisible(false);
        chart.setHorizontalZeroLineVisible(false);
        chart.setVerticalZeroLineVisible(false);
        chart.setMinWidth(getPrimaryScene().getWidth()*.6);
        chart.setMaxHeight(getPrimaryScene().getHeight()*.9);
        chart.getStylesheets().add(chartCSS);
        chart.getData().clear();

        textAreaEmpty = textArea.textProperty().isEmpty();
        alert = ((ConfirmationDialog)applicationTemplate.getDialog(Dialog.DialogType.CONFIRMATION));
        errorDialog = ((ErrorDialog)applicationTemplate.getDialog(Dialog.DialogType.ERROR));
        readOnlyButton = new CheckBox(manager.getPropertyValue(READ_ONLY_TOOLTIP.name()));

        alert.init(applicationTemplate.getUIComponent().getPrimaryWindow());
        errorDialog.init(applicationTemplate.getUIComponent().getPrimaryWindow());

        textArea.setMaxSize(300, 300);
        textArea.setPromptText(manager.getPropertyValue(TEXT_AREA.name()));

        belowText.getChildren().addAll(readOnlyButton);

        //left.getChildren().addAll(textArea, belowText);

        workspace.setPadding(new Insets(20, 20, 20, 20));
        workspace.getChildren().addAll(left, spacer1, verticalDivider, spacer2);


        appPane.getChildren().add(workspace);
    }

    private void setWorkspaceActions() {
        //Checking if we can use new or save buttons
        //newButton.disableProperty().bind(textAreaEmpty);
        saveButton.disableProperty().bind(textAreaEmpty);
        newButton.setDisable(false);

        // Add a listener to check for changes in the textArea
        hasNewText = false;
        hasSaved = false;
        textArea.textProperty().addListener((observable, oldValue, newValue) -> {
            hasNewText = true;
            saveButton.disableProperty().bind(textAreaEmpty);
        });

        algoTypeSelect.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
            generateAlgorithmSelection(newValue);
            algorithmSelectionController(true);
        });
        readOnlyButton.selectedProperty().addListener((observable, oldValue, newValue) -> doneToggled(newValue));


        //Display button
        displayButton.setOnAction(e -> handleDisplayRequest());
    }

    private void handleDisplayRequest() {
        AppData data = (AppData)applicationTemplate.getDataComponent();
        if(hasNewText) {
            hasNewText = false;
            data.loadData(textArea.getText());
            scrnshotButton.setDisable(false);
        }
    }


    public TextArea getTextArea() {
        return textArea;
    }

    public void disableSave() {
        saveButton.disableProperty().unbind();
        saveButton.setDisable(true);
    }

    public void disableSnapshot() {
        scrnshotButton.setDisable(true);
    }

    public void addTextArea() {
        try {
            if(left.getChildren().contains(textArea)) {
                return;
            }
            else {
                workspace.getChildren().remove(spacer1);
                left.getChildren().addAll(textArea, belowText);
            }
        }
        catch (IllegalArgumentException e) {
            // do nothing,
        }
    }

    public void addChart() {
        try {
            if(workspace.getChildren().contains(chart)) {
                return;
            }
            else {
                workspace.getChildren().add(chart);
                workspace.getChildren().remove(spacer2);
            }
        }
        catch (IllegalArgumentException e) {
            // do nothing
        }
    }

    public void removeChart() {
        try {
            if(workspace.getChildren().contains(chart)) {
                workspace.getChildren().remove(chart);
                workspace.getChildren().add(spacer2);
            }
            else {
                return;
            }
        }
        catch (IllegalArgumentException e) {
            // do nothing
        }
    }

    /** Method to reset UI back to a clean slate for adding to again- algorithm selections, loaded info, and graph **/
    public void refreshUI(boolean hardRefresh) {
        textArea.setDisable(false);
        readOnlyButton.setDisable(false);
        readOnlyButton.setSelected(false);
        algoTypeSelect.setValue(null);
        left.getChildren().remove(dataInfo);
        removeAlgorithmControls();
        algorithmTypeSelectionController(false);
        algorithmSelectionController(false);
        if(hardRefresh) {
            clear();
        }
    }

    public void algorithmTypeSelectionController(boolean display) {
        if(display) {
            textArea.setDisable(true);
            if(!(left.getChildren().contains(algoTypeSelect))) {
                left.getChildren().add(algoTypeSelect);
            }
        }
        else {
            textArea.setDisable(false);
            left.getChildren().remove(algoTypeSelect);
        }
    }

    public void generateAlgorithmSelection(AlgorithmTypes type) {
        if(type == null) {
            return;
        }
        PropertyManager manager = PropertyManager.getManager();
        algorithmSelectionController(false);
        removeAlgorithmControls();
        algoSelect = new TilePane();
        algoToggleGroup = new ToggleGroup();
        algorithmSet = ReflectionUtilities.getAlgorithms(type);
        for(AlgorithmWrapper c : algorithmSet) {
            HBox radioBox = new HBox(5);
            RadioButton r = new RadioButton(ReflectionUtilities.trimClassName(c.getName()));

            Button configButton = new Button(manager.getPropertyValue(CONFIGURE_BUTTON.name()));
            configButton.setOnAction(e -> {
                configureAlgorithm(c);
            });

            r.setUserData(c);
            r.setToggleGroup(algoToggleGroup);

            radioBox.getChildren().addAll(r, configButton);
            algoSelect.getChildren().add(radioBox);
        }
        algoToggleGroup.selectedToggleProperty().addListener((observable, oldValue, newValue) -> {
            if (algoToggleGroup.getSelectedToggle() != null) {
                createAlgorithmControls();
                selectedAlgorithm = (AlgorithmWrapper)newValue.getUserData();
                if (((AppData) applicationTemplate.getDataComponent()).getAlgorithmConfig(((AlgorithmWrapper)algoToggleGroup.getSelectedToggle().getUserData())) != null) {
                    runButton.setDisable(false);
                } else {
                    runButton.setDisable(true);
                }
            }
        });
    }

    public void algorithmSelectionController(boolean display) {
        if(display) {
            if(!(left.getChildren().contains(algoSelect))) {
                left.getChildren().add(algoSelect);
            }
        }
        else {
            left.getChildren().remove(algoSelect);
        }
    }

    public void newData() {
        loadedData = false;
        refreshUI(true);
        addTextArea();
        addChart();
    }

    public void loadedData(String dataInfo, String shownData) {
        loadedData = true;
        firstData = false;
        refreshUI(true);
        addTextArea();
        addChart();
        loadDataString(shownData);
        showMetaData(dataInfo);
        readOnlyButton.setDisable(true);
        readOnlyButton.setSelected(true);
        disableSave();
        hasNewText = false;
    }

    public void showMetaData(String data) {
        dataInfo = new Label(data);
        left.getChildren().add(dataInfo);
        dataInfo.setWrapText(true);
    }

    public void doneToggled(boolean on) {
        AppData dataModule = ((AppData)applicationTemplate.getDataComponent());
        if(on) {
            if(!loadedData && !(dataModule.testData(getTextAreaText()))) {
                readOnlyButton.setSelected(false);
                return;
            }
            if(!loadedData) {
                dataModule.loadOriginalData(getTextAreaText());
            }
            String metaData = dataModule.calculateMetaData(dataModule.getLoadedTsd());
            if(!(left.getChildren().contains(dataInfo))) {
                showMetaData(metaData);
            }
            for(AlgorithmTypes type : AlgorithmTypes.values()) {
                if(dataModule.algorithmTypeValidation(type)) {
                    if (!(algoTypeSelect.getItems().contains(type))) {
                        algoTypeSelect.getItems().add(type);
                    }
                }
                else {
                    algoTypeSelect.getItems().remove(type);
                }
            }
            algorithmTypeSelectionController(true);
            if(algoTypeSelect.getItems().isEmpty()) {
                showError(NO_VALID_ALGORITHMS_MSG);
            }
        }
        else {
            refreshUI(false);
        }
    }




    public void configureAlgorithm(AlgorithmWrapper a) {
        AlgorithmConfiguration oldConfig = ((AppData)applicationTemplate.getDataComponent()).getAlgorithmConfig(a);
        AlgorithmConfiguration config = configurationWindow.show(a, oldConfig);
        if(config != null) {
            ((AppData)applicationTemplate.getDataComponent()).addAlgorithmConfiguration(a, config);
            if(left.getChildren().contains(algorithmControls) && algoToggleGroup.getSelectedToggle().getUserData() == a) {
                runButton.setDisable(false);
            }
        }
    }

    public void createAlgorithmControls() {
        if(!(left.getChildren().contains(algorithmControls))) {
            algorithmControls = new HBox(10);

            runButton = new Button();
            runButton.setText(PropertyManager.getManager().getPropertyValue(AppPropertyTypes.DISPLAY_BUTTON.name()));
            runButton.setDisable(true);
            runButton.setOnAction(e -> {
                AppData data = (AppData)(applicationTemplate.getDataComponent());
                data.loadData(getTextAreaText());
                chart.setAnimated(false);
                try {
                    data.prepareAlgorithm(selectedAlgorithm);
                }
                catch(Exception g) {
                    showError(ALGORITHM_ERROR_MSG);
                    return;
                }
                data.runAlgorithm();
            });

            resumeButton = new Button();
            resumeButton.setText(PropertyManager.getManager().getPropertyValue(AppPropertyTypes.RESUME_BUTTON.name()));
            resumeButton.setDisable(true);
            resumeButton.setOnAction(e -> {
                ((AppData)applicationTemplate.getDataComponent()).algorithmResume();
            });

            stopButton = new Button();
            stopButton.setText(PropertyManager.getManager().getPropertyValue(AppPropertyTypes.STOP_BUTTON.name()));
            stopButton.setDisable(true);
            stopButton.setOnAction(e -> {
                stopAlgorithm();
            });

            algorithmControls.getChildren().addAll(runButton, resumeButton, stopButton);
            left.getChildren().add(algorithmControls);
        }
    }

    public void showError(AppPropertyTypes msg) {
        PropertyManager manager = applicationTemplate.manager;
        ErrorDialog error = ((ErrorDialog)applicationTemplate.getDialog(Dialog.DialogType.ERROR));

        error.show(manager.getPropertyValue(ERROR_TITLE.name()), manager.getPropertyValue(msg.name()));
    }

    public void removeAlgorithmControls() {
        left.getChildren().remove(algorithmControls);
    }

    public void loadDataString(String s) {
        textArea.textProperty().setValue(s);
    }

    public String getTextAreaText() {
        return textArea.getText();
    }

    public boolean getFirstData() {
        return firstData;
    }

    public void setFirstData(boolean y) {
        firstData = y;
    }

    public boolean isHasNewText() {
        return hasNewText;
    }

    public void stopAlgorithm() {
        ((AppData)applicationTemplate.getDataComponent()).algorithmStop();
        left.getChildren().remove(stopping);
        stopping = new Label();
        stopping.setText(PropertyManager.getManager().getPropertyValue(STOPPING_MESSAGE.name()));
        left.getChildren().add(stopping);
    }

    public ApplicationState getApplicationState() {
        return applicationState;
    }

    public boolean stateIsRunning() {
        return getApplicationState() == ApplicationState.ALGORITHM_RUNNING || getApplicationState() == ApplicationState.ALGORITHM_PAUSED;
    }

    public void setApplicationState(ApplicationState applicationState) {
        if(applicationState == ApplicationState.ALGORITHM_RUNNING) {
            algoTypeSelect.setDisable(true);
            algoSelect.setDisable(true);
            runButton.setDisable(true);
            resumeButton.setDisable(true);
            stopButton.setDisable(false);
            newButton.setDisable(true);
            loadButton.setDisable(true);
            scrnshotButton.setDisable(true);
            readOnlyButton.setDisable(true);
        }
        else if(applicationState == ApplicationState.SETUP) {
            algoTypeSelect.setDisable(false);
            algoSelect.setDisable(false);
            newButton.setDisable(false);
            loadButton.setDisable(false);
            scrnshotButton.setDisable(true);
            stopButton.setDisable(true);
        }
        else if(applicationState == ApplicationState.ALGORITHM_FINISHED) {
            algoTypeSelect.setDisable(false);
            algoSelect.setDisable(false);
            runButton.setDisable(false);
            resumeButton.setDisable(true);
            newButton.setDisable(false);
            loadButton.setDisable(false);
            scrnshotButton.setDisable(false);
            stopButton.setDisable(true);
            if(!loadedData) {
                readOnlyButton.setDisable(false);
            }
            Platform.runLater(() -> left.getChildren().remove(stopping));
        }
        else if(applicationState == ApplicationState.ALGORITHM_PAUSED) {
            resumeButton.setDisable(false);
            scrnshotButton.setDisable(false);
        }
        this.applicationState = applicationState;
    }

}
