package ui;

import data.AlgorithmConfiguration;
import data.AlgorithmWrapper;
import javafx.event.ActionEvent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import settings.AlgorithmTypes;
import settings.AppPropertyTypes;
import vilij.components.Dialog;
import vilij.components.ErrorDialog;
import vilij.propertymanager.PropertyManager;

import java.util.Arrays;
import java.util.List;

public class AlgorithmConfigurationWindow extends Stage implements Dialog {

    private static AlgorithmConfigurationWindow config;

    private AlgorithmConfigurationWindow() { /* empty constructor */ }

    public enum Option {

        APPLY("Apply"), NO("No"), CANCEL("Cancel");

        @SuppressWarnings("unused")
        private String option;

        Option(String option) { this.option = option; }
    }

    private AlgorithmConfiguration resultant;
    PropertyManager manager = PropertyManager.getManager();

    // Controls
    private TextField iterations = new TextField();
    private TextField updates = new TextField();
    private TextField labels = new TextField();
    private CheckBox  continuous = new CheckBox();

    // Labels
    private Label    iterationsLabel = new Label(manager.getPropertyValue(AppPropertyTypes.ITERATIONS_LABEL.name()));
    private Label    updateLabel = new Label(manager.getPropertyValue(AppPropertyTypes.UPDATES_LABEL.name()));
    private Label    labelsLabel = new Label(manager.getPropertyValue(AppPropertyTypes.LABELS_LABEL.name()));
    private Label    continuousLabel= new Label(manager.getPropertyValue(AppPropertyTypes.CONTINUOUS_LABEL.name()));

    private Option selectedOption;

    public static AlgorithmConfigurationWindow getConfigWindow () {
        if (config == null)
            config = new AlgorithmConfigurationWindow();
        return config;
    }

    boolean auditData() {
        AlgorithmConfiguration config = new AlgorithmConfiguration();
        try {
            config.setIterations(Integer.parseInt(iterations.getText()));
            config.setInterval(Integer.parseInt(updates.getText()));
            config.setMaxLabels(Integer.parseInt(labels.getText()));
            config.setContinuous(continuous.isSelected());
        }
        catch(Exception e) {
            ErrorDialog error = ErrorDialog.getDialog();
            error.show(manager.getPropertyValue(AppPropertyTypes.ERROR_TITLE.name()), manager.getPropertyValue(AppPropertyTypes.DATA_AUDIT_FAILED_MESSAGE.name()));
            return false;
        }
        if(config.getInterval() <= 0 || config.getIterations() <= 0 || config.getMaxLabels() < 2) {
            ErrorDialog error = ErrorDialog.getDialog();
            error.show(manager.getPropertyValue(AppPropertyTypes.ERROR_TITLE.name()), manager.getPropertyValue(AppPropertyTypes.DATA_AUDIT_FAILED_MESSAGE.name()));
            return false;
        }
        resultant = config;
        return true;
    }

    void handleExit() {
        if(selectedOption == Option.APPLY) {
            if(auditData()) {
                this.hide();
            }
        }
        else {
            this.hide();
        }
    }

    @Override
    public void init(Stage owner) {
        initModality(Modality.WINDOW_MODAL); // modal => messages are blocked from reaching other windows
        initOwner(owner);

        List<Button> buttons = Arrays.asList(
                new Button(Option.APPLY.name()),
                new Button(Option.NO.option),
                new Button(Option.CANCEL.name()));

        buttons.forEach(button -> button.setOnAction((ActionEvent event) -> {
            this.selectedOption = Option.valueOf(((Button) event.getSource()).getText());
            this.handleExit();
        }));

        iterations.textProperty().addListener((observable, oldValue, newValue) -> {
            try {
                Integer.parseInt(iterations.textProperty().getValue());
            }
            catch (Exception e) {
                iterations.setText(oldValue);
            }
            if(Integer.parseInt(iterations.textProperty().getValue()) <= 0) {
                iterations.setText(oldValue);
            }
        });

        updates.textProperty().addListener((observable, oldValue, newValue) -> {
            try {
                Integer.parseInt(updates.textProperty().getValue());
            }
            catch (Exception e) {
                updates.setText(oldValue);
            }
            if(Integer.parseInt(updates.textProperty().getValue()) <= 0) {
                updates.setText(oldValue);
            }
        });

        labels.textProperty().addListener((observable, oldValue, newValue) -> {
            try {
                Integer.parseInt(labels.textProperty().getValue());
            }
            catch (Exception e) {
                labels.setText(oldValue);
            }
            if(Integer.parseInt(labels.textProperty().getValue()) < 2) {
                labels.setText(oldValue);
            }
            if(Integer.parseInt(labels.textProperty().getValue()) > 4) {
                labels.setText(oldValue);
            }
        });

        GridPane grid = new GridPane();

        grid.add(iterationsLabel, 0, 0);
        grid.add(iterations, 1, 0);

        grid.add(updateLabel, 0, 1);
        grid.add(updates, 1, 1);

        grid.add(continuousLabel, 0, 2);
        grid.add(continuous,1, 2);

        grid.add(labelsLabel, 0, 3);
        grid.add(labels,1,3);

        grid.add(buttons.get(0), 0, 4);
        grid.add(buttons.get(2), 1, 4);

        this.setScene(new Scene(grid));
    }

    @Override
    public void show(String title, String message) {
        // Unneeded
    }

    public AlgorithmConfiguration show(AlgorithmWrapper a, AlgorithmConfiguration config) {
        resultant = null;
        if(a.getType() == AlgorithmTypes.CLASSIFICATION) {
            labels.setVisible(false);
            labelsLabel.setVisible(false);
        }
        else {
            labels.setVisible(true);
            labelsLabel.setVisible(true);
        }
        if(config != null) {
            iterations.setText(Integer.toString(config.getIterations()));
            updates.setText(Integer.toString(config.getInterval()));
            labels.setText(Integer.toString(config.getMaxLabels()));
            continuous.setSelected(config.isContinuous());
        }
        else {
            iterations.setText("1");
            updates.setText("1");
            labels.setText("2");
            continuous.setSelected(false);
        }
        showAndWait();                   // open the dialog and wait for the user to click the close button
        return resultant;
    }

}
