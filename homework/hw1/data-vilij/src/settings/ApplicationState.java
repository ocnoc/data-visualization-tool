package settings;

public enum ApplicationState {
    SETUP, ALGORITHM_RUNNING, ALGORITHM_PAUSED, ALGORITHM_FINISHED
}
