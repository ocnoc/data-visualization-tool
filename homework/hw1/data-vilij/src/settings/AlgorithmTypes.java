package settings;

import algorithms.Classifier;
import algorithms.Clusterer;
import vilij.propertymanager.PropertyManager;

import static settings.AppPropertyTypes.CLASSIFICATION_TYPE;
import static settings.AppPropertyTypes.CLUSTERING_TYPE;

public enum AlgorithmTypes {
    CLASSIFICATION(CLASSIFICATION_TYPE, Classifier.class, 2, 0),
    CLUSTERING(CLUSTERING_TYPE, Clusterer.class, 0, 0);

    private final String label;
    private final Class type;
    private final int minLabels;
    private final int minInstances;

    AlgorithmTypes(AppPropertyTypes e, Class type, int minLabels, int minInstances) {
        PropertyManager pm = vilij.propertymanager.PropertyManager.getManager();
        this.label = pm.getPropertyValue(e.name());
        this.type = type;
        this.minLabels = minLabels;
        this.minInstances = minInstances;
    }

    public String toString() {
        return label;
    }

    public String getAlgorithmPackage() {
        return toString().toLowerCase();
    }

    public Class getAlgorithmClass() {
        return type;
    }

    public int getMinLabels() {
        return minLabels;
    }

    public int getMinInstances() {
        return minInstances;
    }

}
