package settings;

/**
 * This enumerable type lists the various application-specific property types listed in the initial set of properties to
 * be loaded from the workspace properties <code>xml</code> file specified by the initialization parameters.
 *
 * @author Ritwik Banerjee
 * @see vilij.settings.InitializationParams
 */
public enum AppPropertyTypes {

    /* resource files and folders */
    DATA_RESOURCE_PATH,
    ICON_RESOURCE_PATH,
    CHART_CSS,

    /* user interface icon file names */
    SCREENSHOT_ICON,
    CONFIGURATION_ICON,

    /* tooltips for user interface buttons */
    SCREENSHOT_TOOLTIP,
    READ_ONLY_TOOLTIP,
    ALGORITHM_TYPE_SELECTION_TOOLTIP,

    /* Titles for user interface buttons */
    DISPLAY_BUTTON,
    CONFIGURE_BUTTON,
    RESUME_BUTTON,
    STOP_BUTTON,

    /* error messages */
    RESOURCE_SUBDIR_NOT_FOUND,
    DISPLAY_ERROR,
    MANY_LINES,
    SCREENSHOT_ERROR,
    NAME_ERROR_MESSAGE,
    NAME_DUPE_MESSAGE,
    FORMAT_ERROR_MESSAGE,
    DATA_AUDIT_FAILED_MESSAGE,
    ERROR_TITLE,
    ALGORITHM_ERROR_MSG,
    NO_VALID_ALGORITHMS_MSG,
    DIRECTORY_ERROR_MSG,

    /* application-specific message titles */
    SAVE_UNSAVED_WORK_TITLE,
    CHART_TITLE,
    EXIT_TITLE,
    DISPLAY_ERROR_TITLE,
    LOAD_WORK_TITLE,
    MANY_LINES_TITLE,
    FORMAT_ERROR_TITLE,
    SCREENSHOT_ERROR_TITLE,
    SCREENSHOT_PICKER,

    /* application-specific messages */
    SAVE_UNSAVED_WORK,
    EXIT_WHILE_RUNNING_WARNING,
    METADATA_MAIN,
    METADATA_SOURCE,
    METADATA_LABELS,
    STOPPING_MESSAGE,

    /* application-specific parameters */
    PNG_FILE_EXT,
    PNG_FILE_EXT_DESC,
    IMAGE_FORMAT,
    DATA_FILE_EXT,
    DATA_FILE_EXT_DESC,
    TEXT_AREA,
    SPECIFIED_FILE,

    /* Configuration Window Labels */
    ITERATIONS_LABEL,
    UPDATES_LABEL,
    CONTINUOUS_LABEL,
    LABELS_LABEL,

    /* Algorithm Types */
    CLASSIFICATION_TYPE,
    CLUSTERING_TYPE,

    /*Algorithms*/
    RANDOM_CLASSIFIER,
    RANDOM_CLUSTERING
}
