package actions;

import dataprocessors.AppData;
import dataprocessors.TSDProcessor;
import javafx.embed.swing.SwingFXUtils;
import javafx.scene.Node;
import javafx.scene.SnapshotParameters;
import javafx.scene.image.WritableImage;
import javafx.stage.FileChooser;
import settings.AppPropertyTypes;
import settings.ApplicationState;
import ui.AppUI;
import vilij.components.ActionComponent;
import vilij.components.ConfirmationDialog;
import vilij.components.Dialog;
import vilij.components.ErrorDialog;
import vilij.propertymanager.PropertyManager;
import vilij.templates.ApplicationTemplate;

import javax.imageio.ImageIO;
import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;

import static settings.AppPropertyTypes.*;
import static vilij.settings.PropertyTypes.*;

/**
 * This is the concrete implementation of the action handlers required by the application.
 *
 * @author Ritwik Banerjee
 */
public final class AppActions implements ActionComponent {

    /** The application to which this class of actions belongs. */
    private ApplicationTemplate applicationTemplate;

    /** Path to the data file currently active. */
    private Path dataFilePath;
    private TSDProcessor processor;

    private final static String SEPARATOR = "/";

    public AppActions(ApplicationTemplate applicationTemplate) {
        this.processor = new TSDProcessor();
        this.applicationTemplate = applicationTemplate;
    }

    @Override
    public void handleNewRequest() {
        PropertyManager manager = applicationTemplate.manager;
        ErrorDialog error = ((ErrorDialog)applicationTemplate.getDialog(Dialog.DialogType.ERROR));
        boolean saved;
        if(((AppUI)applicationTemplate.getUIComponent()).getFirstData()) {
            try {
                saved = promptToSave();
            } catch (IOException e) {
                error.show(manager.getPropertyValue(SAVE_ERROR_TITLE.name()), manager.getPropertyValue(SAVE_ERROR_MSG.name()));
                return;
            }
        }
        else {
            saved = true;
            ((AppUI)applicationTemplate.getUIComponent()).setFirstData(true);
        }
        if(saved) {
            ((AppData)applicationTemplate.getDataComponent()).getLoadedData().clear();
            dataFilePath = null;
            applicationTemplate.getUIComponent().clear();
            ((AppUI)applicationTemplate.getUIComponent()).setApplicationState(ApplicationState.SETUP);
            ((AppUI)applicationTemplate.getUIComponent()).disableSnapshot();
            ((AppUI)applicationTemplate.getUIComponent()).newData();
        }

    }

    @Override
    public void handleSaveRequest() {
        PropertyManager manager = applicationTemplate.manager;
        ErrorDialog error = ((ErrorDialog)applicationTemplate.getDialog(Dialog.DialogType.ERROR));
        try {
            promptToSave();
        }
        catch (IOException e) {
            error.show(manager.getPropertyValue(SAVE_ERROR_TITLE.name()), manager.getPropertyValue(SAVE_ERROR_MSG.name()));
        }


    }

    @Override
    public void handleLoadRequest() {
        PropertyManager manager = applicationTemplate.manager;
        ErrorDialog error = ((ErrorDialog)applicationTemplate.getDialog(Dialog.DialogType.ERROR));
        try {
            promptToLoad();
            ((AppUI)applicationTemplate.getUIComponent()).setApplicationState(ApplicationState.SETUP);
        }
        catch (Exception e) {
            e.printStackTrace();
            error.show(manager.getPropertyValue(LOAD_ERROR_TITLE.name()), manager.getPropertyValue(LOAD_ERROR_MSG.name()));
        }
    }

    @Override
    public void handleExitRequest() {
        ConfirmationDialog alert = ((ConfirmationDialog)applicationTemplate.getDialog(Dialog.DialogType.CONFIRMATION));
        PropertyManager manager = applicationTemplate.manager;

        if(((AppUI)applicationTemplate.getUIComponent()).stateIsRunning()) {
            alert.show(manager.getPropertyValue(EXIT_TITLE.name()), manager.getPropertyValue(EXIT_WHILE_RUNNING_WARNING.name()));


            ConfirmationDialog.Option answer = alert.getSelectedOption();
            if (answer == ConfirmationDialog.Option.YES) {
                System.exit(0);
            }
        }

        else if(((AppUI)applicationTemplate.getUIComponent()).isHasNewText()) {
            try {
                boolean saved = promptToSave();
                if(saved) {
                    System.exit(0);
                }
            }
            catch (IOException e) {
                // idek
            }
        }

        else {
            System.exit(0);
        }
    }

    @Override
    public void handlePrintRequest() {
        // TODO: NOT A PART OF HW 1
    }

    public void handleScreenshotRequest() throws IOException {
        PropertyManager manager = applicationTemplate.manager;
        FileChooser picker = new FileChooser();
        String dataPath = SEPARATOR + String.join(SEPARATOR, manager.getPropertyValue(DATA_RESOURCE_PATH.name()));
        File file = new File(getClass().getResource(dataPath).getPath());
        ErrorDialog error = ((ErrorDialog)applicationTemplate.getDialog(Dialog.DialogType.ERROR));

        picker.setTitle(manager.getPropertyValue(SCREENSHOT_PICKER.name()));
        picker.setInitialDirectory(file);
        picker.getExtensionFilters().add(
                new FileChooser.ExtensionFilter(manager.getPropertyValue(PNG_FILE_EXT_DESC.name()), manager.getPropertyValue(PNG_FILE_EXT.name()))
        );

        try {
            file = picker.showSaveDialog(applicationTemplate.getUIComponent().getPrimaryWindow());
        } catch (Exception e) {
            error.show(manager.getPropertyValue(RESOURCE_SUBDIR_NOT_FOUND.name()), manager.getPropertyValue(RESOURCE_SUBDIR_NOT_FOUND.name()));
            throw new IOException();
        }
        if(file != null) {
            Node node = ((AppUI)applicationTemplate.getUIComponent()).getChart();
            WritableImage img = node.snapshot(new SnapshotParameters(), null);
            ImageIO.write(SwingFXUtils.fromFXImage(img, null), manager.getPropertyValue(IMAGE_FORMAT.name()), file);
        }
    }


    /**
     * This helper method verifies that the user really wants to save their unsaved work, which they might not want to
     * do. The user will be presented with three options:
     * <ol>
     * <li><code>yes</code>, indicating that the user wants to save the work and continue with the action,</li>
     * <li><code>no</code>, indicating that the user wants to continue with the action without saving the work, and</li>
     * <li><code>cancel</code>, to indicate that the user does not want to continue with the action, but also does not
     * want to save the work at this point.</li>
     * </ol>
     *
     * @return <code>false</code> if the user presses the <i>cancel</i>, and <code>true</code> otherwise.
     */
    private boolean promptToSave() throws IOException {
        PropertyManager manager = applicationTemplate.manager;
        FileChooser picker = new FileChooser();
        String dataPath = SEPARATOR + String.join(SEPARATOR, manager.getPropertyValue(DATA_RESOURCE_PATH.name()));
        File file = new File(getClass().getResource(dataPath).getPath());
        ErrorDialog error = ((ErrorDialog)applicationTemplate.getDialog(Dialog.DialogType.ERROR));
        ConfirmationDialog alert = ((ConfirmationDialog)applicationTemplate.getDialog(Dialog.DialogType.CONFIRMATION));
        alert.show(manager.getPropertyValue(SAVE_UNSAVED_WORK_TITLE.name()), manager.getPropertyValue(SAVE_UNSAVED_WORK.name()));
        StringBuilder dataCheck = new StringBuilder(((AppUI)applicationTemplate.getUIComponent()).getTextArea().getText());
        ArrayList<String> hiddenData = ((AppData)applicationTemplate.getDataComponent()).getLoadedData();

        picker.setTitle(manager.getPropertyValue(SAVE_WORK_TITLE.name()));
        picker.setInitialDirectory(file);
        picker.setInitialFileName(manager.getPropertyValue(SPECIFIED_FILE.name()));
        picker.getExtensionFilters().add(
                    new FileChooser.ExtensionFilter(manager.getPropertyValue(DATA_FILE_EXT_DESC.name()), manager.getPropertyValue(DATA_FILE_EXT.name()))
        );
        if(alert.getSelectedOption() == ConfirmationDialog.Option.YES) {
            try {
                if(!(dataCheck.toString().substring(dataCheck.toString().length() - 1).equals(System.lineSeparator()))) {
                    dataCheck.append(System.lineSeparator());
                }
                hiddenData.forEach(dataCheck::append);
                processor.processString(dataCheck.toString());
            }
            catch (Exception e) {
                String title = manager.getPropertyValue(FORMAT_ERROR_TITLE.name());
                String msg = e.getMessage();
                error.show(title, msg);
                return false;
            }
            if (dataFilePath == null) {
                if(dataPath == null) {
                    throw new IOException(manager.getPropertyValue(AppPropertyTypes.RESOURCE_SUBDIR_NOT_FOUND.name()));
                }
                try {
                    file = picker.showSaveDialog(applicationTemplate.getUIComponent().getPrimaryWindow());
                } catch (Exception e) {
                    error.show(manager.getPropertyValue(RESOURCE_SUBDIR_NOT_FOUND.name()), manager.getPropertyValue(RESOURCE_SUBDIR_NOT_FOUND.name()));
                    return false;
                }
                if (file != null) {
                    dataFilePath = Paths.get(file.getPath());
                    save();
                }
                else {
                    return false;
                }
            }
            else {
                save();
            }
            return true;
        }
        else return alert.getSelectedOption() == ConfirmationDialog.Option.NO;
    }

    private boolean promptToLoad() {
        PropertyManager manager = applicationTemplate.manager;
        FileChooser picker = new FileChooser();
        String dataPath = SEPARATOR + String.join(SEPARATOR, manager.getPropertyValue(DATA_RESOURCE_PATH.name()));
        File file = new File(getClass().getResource(dataPath).getPath());

        picker.setTitle(manager.getPropertyValue(LOAD_WORK_TITLE.name()));
        picker.setInitialDirectory(file);
        picker.setInitialFileName(manager.getPropertyValue(SPECIFIED_FILE.name()));
        picker.getExtensionFilters().add(
                new FileChooser.ExtensionFilter(manager.getPropertyValue(DATA_FILE_EXT_DESC.name()), manager.getPropertyValue(DATA_FILE_EXT.name()))
        );

        file = picker.showOpenDialog(applicationTemplate.getUIComponent().getPrimaryWindow());
        if(file != null) {
            dataFilePath = Paths.get(file.getPath());
            applicationTemplate.getDataComponent().loadData(dataFilePath);
            return true;
        }
        return false;
    }

    private void save() {
        applicationTemplate.getDataComponent().saveData(dataFilePath);
        ((AppUI)applicationTemplate.getUIComponent()).disableSave();
    }

}
