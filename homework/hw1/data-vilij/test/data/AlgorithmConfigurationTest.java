package data;

import org.junit.Test;

import static org.junit.Assert.*;

public class AlgorithmConfigurationTest {

    /**
     * 0 is an invalid value, but gets replaced with 1 by the setter.
     */
    @Test
    public void testInvalidIterations() {
        AlgorithmConfiguration config = new AlgorithmConfiguration();

        config.setIterations(0);
        assertEquals(config.getIterations(), 1);
    }

    /**
     * 1 is the lowest valid value for this field.
     */
    @Test
    public void testBorderlineIterations() {
        AlgorithmConfiguration config = new AlgorithmConfiguration();

        config.setIterations(1);
        assertEquals(config.getIterations(), 1);
    }

    /**
     * 500 is a perfectly average number to use for this field.
     */
    @Test
    public void testNormalIterations() {
        AlgorithmConfiguration config = new AlgorithmConfiguration();

        config.setIterations(500);
        assertEquals(config.getIterations(), 500);
    }

    /**
     * 0 is an invalid number in this field, it gets replaced with 1
     */
    @Test
    public void testInvalidInterval() {
        AlgorithmConfiguration config = new AlgorithmConfiguration();

        config.setInterval(0);
        assertEquals(config.getInterval(), 1);
    }

    /**
     * 1 is the lowest possible number for this field
     */
    @Test
    public void testBorderlineInterval() {
        AlgorithmConfiguration config = new AlgorithmConfiguration();

        config.setInterval(1);
        assertEquals(config.getInterval(), 1);
    }

    /**
     * 500 is a perfectly average number for this field.
     */
    @Test
    public void testNormalInterval() {
        AlgorithmConfiguration config = new AlgorithmConfiguration();

        config.setInterval(500);
        assertEquals(config.getInterval(), 500);
    }

    /**
     * 0 is an invalid number for this field, it gets replaced with 2, the lowest possible.
     */
    @Test
    public void testInvalidLabels() {
        AlgorithmConfiguration config = new AlgorithmConfiguration();

        config.setMaxLabels(0);
        assertEquals(config.getMaxLabels(), 2);
    }

    /**
     * 2 is the lowest possible value for this field
     */
    @Test
    public void testBorderlineLabels() {
        AlgorithmConfiguration config = new AlgorithmConfiguration();

        config.setMaxLabels(2);
        assertEquals(config.getMaxLabels(), 2);
    }

    /**
     * 4 is the highest possible number for this field, though 3 will work just as well. Anything above 4 will be replaced with 4
     */
    @Test
    public void testNormalLabels() {
        AlgorithmConfiguration config = new AlgorithmConfiguration();

        config.setMaxLabels(4);
        assertEquals(config.getMaxLabels(), 4);
    }

    /**
     * This is a boolean, not much to say.
     */
    @Test
    public void testContinuous() {
        AlgorithmConfiguration config = new AlgorithmConfiguration();

        config.setContinuous(true);
        assertTrue(config.isContinuous());
    }

    /**
     * There's no degradation in place if an invalid boolean is passed through, so doing something like casting an object to boolean will result in an exception
     */
    @Test(expected = ClassCastException.class)
    public void testInvalidContinuous() {
        AlgorithmConfiguration config = new AlgorithmConfiguration();

        config.setContinuous((boolean)new Object());
    }
}
