package data;

import algorithms.Algorithm;
import algorithms.Classifier;
import algorithms.Clusterer;
import dataprocessors.AlgorithmFactory;
import dataprocessors.ReflectionUtilities;
import org.junit.Test;
import settings.AlgorithmTypes;
import vilij.propertymanager.PropertyManager;

import java.util.HashSet;

import static org.junit.Assert.*;

public class AlgorithmTest {

    /**
     * Iterates through every algorithm available and tests a good config with it.
     * The config tested has all acceptable values.
     * Clustering is tested with maxLabels, while classification algorithms don't accept that.
     */
    @Test
    public void testGoodConfigs() {
        PropertyManager pm = PropertyManager.getManager();
        pm.addProperty("CLUSTERING_TYPE", "Clustering");
        pm.addProperty("CLASSIFICATION_TYPE", "Classification");

        AlgorithmConfiguration config = new AlgorithmConfiguration();
        config.setContinuous(true);
        config.setInterval(5);
        config.setIterations(10);
        config.setMaxLabels(3);
        HashSet<AlgorithmWrapper> algorithms = new HashSet<>();
        for (AlgorithmTypes type : AlgorithmTypes.values()) {
            algorithms.addAll(ReflectionUtilities.getAlgorithms(type));
        }
        algorithms.forEach(wrapper -> {
            Algorithm result = AlgorithmFactory.getAlgorithm(wrapper, config, null);
            assertEquals(result.getMaxIterations(), 10);
            assertEquals(result.getUpdateInterval(), 5);
            assertTrue(result.tocontinue());
            if (wrapper.getType() == AlgorithmTypes.CLUSTERING) {
                assertEquals(((Clusterer) result).getNumberOfClusters(), 3);
            }
        });
    }

    /**
     * This configurations has every value at the limits of what is allowed by the application. Interval and Iterations
     * cap out at Integer.MAX_INT, while maxLabels has al lower bound of 2.
     */
    @Test
    public void testBorderlineConfigs() {
        PropertyManager pm = PropertyManager.getManager();
        pm.addProperty("CLUSTERING_TYPE", "Clustering");
        pm.addProperty("CLASSIFICATION_TYPE", "Classification");

        AlgorithmConfiguration config = new AlgorithmConfiguration();
        config.setContinuous(true);
        config.setInterval(Integer.MAX_VALUE);
        config.setIterations(Integer.MAX_VALUE);
        config.setMaxLabels(2);
        HashSet<AlgorithmWrapper> algorithms = new HashSet<>();
        for (AlgorithmTypes type : AlgorithmTypes.values()) {
            algorithms.addAll(ReflectionUtilities.getAlgorithms(type));
        }
        algorithms.forEach(wrapper -> {
            Algorithm result = AlgorithmFactory.getAlgorithm(wrapper, config, null);
            assertEquals(result.getMaxIterations(), Integer.MAX_VALUE);
            assertEquals(result.getUpdateInterval(), Integer.MAX_VALUE);
            assertTrue(result.tocontinue());
            if (wrapper.getType() == AlgorithmTypes.CLUSTERING) {
                assertEquals(((Clusterer) result).getNumberOfClusters(), 2);
            }
        });
    }

    /**
     * This is an attempt to generate algorithms with a config that takes invalid input, but degrades those inputs
     * to valid values.
     * Negatives are never acceptable, and 5 is greater than the maxLabels cap of 4.
     * These values are all reworked to their closest valid values though, and the algorithms can be constructed fine.
     */
    @Test
    public void testDegradeableInvalidConfigs() {
        PropertyManager pm = PropertyManager.getManager();
        pm.addProperty("CLUSTERING_TYPE", "Clustering");
        pm.addProperty("CLASSIFICATION_TYPE", "Classification");

        AlgorithmConfiguration config = new AlgorithmConfiguration();
        config.setContinuous(true);
        config.setInterval(-5);
        config.setIterations(-6);
        config.setMaxLabels(5);
        HashSet<AlgorithmWrapper> algorithms = new HashSet<>();
        for (AlgorithmTypes type : AlgorithmTypes.values()) {
            algorithms.addAll(ReflectionUtilities.getAlgorithms(type));
        }
        algorithms.forEach(wrapper -> {
            Algorithm result = AlgorithmFactory.getAlgorithm(wrapper, config, null);
            assertEquals(result.getMaxIterations(), 1);
            assertEquals(result.getUpdateInterval(), 1);
            assertTrue(result.tocontinue());
            if (wrapper.getType() == AlgorithmTypes.CLUSTERING) {
                assertEquals(((Clusterer) result).getNumberOfClusters(), 4);
            }
        });
    }

    /**
     * This is an example of a configuration that is NOT degradeable and will cause an exception.
     * A string inputted for an integer value will cause an exception to be thrown. As this happens at creating the configuration,
     * the application will never attempt to create the algorithm.
     */
    @Test(expected = NumberFormatException.class)
    public void testNonDegradeableInvalidConfigs() {
        PropertyManager pm = PropertyManager.getManager();
        pm.addProperty("CLUSTERING_TYPE", "Clustering");
        pm.addProperty("CLASSIFICATION_TYPE", "Classification");

        AlgorithmConfiguration config = new AlgorithmConfiguration();
        config.setContinuous(true);
        config.setInterval(Integer.parseInt("Not an integer"));
        config.setIterations(-6);
        config.setMaxLabels(5);
        HashSet<AlgorithmWrapper> algorithms = new HashSet<>();
        for (AlgorithmTypes type : AlgorithmTypes.values()) {
            algorithms.addAll(ReflectionUtilities.getAlgorithms(type));
        }
        algorithms.forEach(wrapper -> {
            Algorithm result = AlgorithmFactory.getAlgorithm(wrapper, config, null);
            assertEquals(result.getMaxIterations(), 1);
            assertEquals(result.getUpdateInterval(), 2);
            assertTrue(result.tocontinue());
            if (wrapper.getType() == AlgorithmTypes.CLUSTERING) {
                assertEquals(((Clusterer) result).getNumberOfClusters(), 4);
            }
        });
    }
}