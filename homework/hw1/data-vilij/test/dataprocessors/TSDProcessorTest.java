package dataprocessors;

import javafx.geometry.Point2D;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.util.HashMap;

import static org.junit.Assert.*;

public class TSDProcessorTest {

    /**
     * This is an example instance name boundary value. A blank instance name is invalid, and all must start with @,
     * so the most simplistic allowed would be just "@".
     */
    @Test
    public void testProcessStringName() {
        TSDProcessor testProcessor = new TSDProcessor();
        String tsdString;
        HashMap<String, String> expectedLabel;
        HashMap<String, Point2D> expectedPoint;

        // Testing with only "@" for name
        testProcessor.clear();
        tsdString = "@\tnull\t0,0";
        try {
            testProcessor.processString(tsdString);
        } catch (Exception e) {
            //do nothing ,test will probably fail?
        }
        expectedLabel = new HashMap<>();
        expectedLabel.put("@", "null");
        assertEquals(expectedLabel, testProcessor.getDataLabels());

    }

    /**
     * This is an example of a boundary value for the location of the data. The max allowed would be Integer.MAX_VALUE, and
     * the lowest allowed would be Integer.MIN_VALUE
     */
    @Test
    public void testProcessStringMinMax() {
        TSDProcessor testProcessor = new TSDProcessor();
        String tsdString;
        HashMap<String, Point2D> expectedPoint;

        // Testing min/max java integers
        int x = Integer.MAX_VALUE;
        int y = Integer.MIN_VALUE;
        tsdString = "@\tnull\t" + x + "," + y;
        try {
            testProcessor.processString(tsdString);
        } catch (Exception e) {
            //do nothing ,test will probably fail?
        }
        expectedPoint = new HashMap<>();
        expectedPoint.put("@", new Point2D(x, y));
        assertEquals(expectedPoint, testProcessor.getDataPoints());
    }

    @Test (expected = Exception.class)
    public void tsdProcessEmptyLabelString() throws Exception {
        TSDProcessor testProcessor = new TSDProcessor();
        String emptyLabel = "@instance1\t\t1,1";

        testProcessor.processString(emptyLabel);
    }



}