package dataprocessors;

import javafx.scene.control.TextArea;
import org.junit.Test;
import ui.AppUI;
import vilij.propertymanager.PropertyManager;

import java.io.*;
import java.nio.file.Paths;

import static org.junit.Assert.*;
import static settings.AppPropertyTypes.DATA_RESOURCE_PATH;

public class AppDataTest {

    /**
     * This is a standard line of data being saved to a valid file (located in testing resources).
     * @throws IOException
     */
    @Test
    public void testSaveDataProper() throws IOException {
        AppData data = new AppData(null);
        StringBuilder dataString = new StringBuilder("@a\tnull\t1,1");
        String SEPARATOR = "/";
        String dataPath = String.join(SEPARATOR, "data", "test.tsd");
        ClassLoader classLoader = getClass().getClassLoader();
//        File test = new File(classLoader.getResource("data/test.tsd").getFile());
        File file = new File(classLoader.getResource(dataPath).getFile());

        data.saveData(Paths.get(file.getPath()), dataString);
        String result = new BufferedReader(new FileReader(file)).readLine();
        assertEquals("@a\tnull\t1,1", result);
    }

    /**
     * This is a valid line of data being saved to an invalid location.
     * It will thorw a null pointer exception before hitting IOErrors due to the File creator.
     * @throws IOException
     */
    @Test(expected = NullPointerException.class)
    public void testSaveDataImproper() throws IOException {
        AppData data = new AppData(null);
        StringBuilder dataString = new StringBuilder("@a\tnull\t1,1");
        String SEPARATOR = "/";
        String dataPath = String.join(SEPARATOR, "dat", "test.tsd");
        ClassLoader classLoader = getClass().getClassLoader();
//        File test = new File(classLoader.getResource("data/test.tsd").getFile());
        File file = new File(classLoader.getResource(dataPath).getFile());

        data.saveData(Paths.get(file.getPath()), dataString);
        String result = new BufferedReader(new FileReader(file)).readLine();
        assertEquals("@a\tnull\t1,1", result);
    }
}