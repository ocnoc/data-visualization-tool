Add algorithms by placing them in the algorithms folder. Algorithms are .java files that implement the methods present in the Algorithm interface, and depending on the type,
the Classification or Clustering interfaces.

Look at the default algorithms for examples on how to properly implement the necessary methods.

Datasets can be pasted into the program upon loading or loaded from a file.